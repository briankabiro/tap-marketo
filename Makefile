.PHONY: test test-quick run build mypy

APP_NAME=tap-marketo
CONFIG_FILE=config.json

clean:
	@rm -rf dist/ build/ {,*/}*.egg-info

sdist: clean
	@python setup.py sdist

build: clean
	@echo "Building app..."
	@time docker build -t ${APP_NAME} .

test: build
	@echo "Running full test suite..."
	@time docker run \
					-e MARKETO_ENDPOINT -e MARKETO_IDENTITY \
					-e MARKETO_CLIENT_SECRET -e MARKETO_CLIENT_ID \
					${APP_NAME}  pytest -vv -x

test-quick: build
	@echo "Running tests that don't require a real API..."
	@time docker run ${APP_NAME} pytest -vv -k TestMarketoUtils

run: build
	@echo "Running tap-marketo..."
	@time docker run \
					-e MARKETO_ENDPOINT -e MARKETO_IDENTITY \
					-e MARKETO_CLIENT_SECRET -e MARKETO_CLIENT_ID \
					${APP_NAME}  /bin/bash -c "tap-marketo -c $CONFIG_FILE create_keyfile --minute_offset 30; tap-marketo -c $CONFIG_FILE extract"

dry-run: build
	@echo "Running tap-marketo..."
	@time docker run \
					-e MARKETO_ENDPOINT -e MARKETO_IDENTITY \
					-e MARKETO_CLIENT_SECRET -e MARKETO_CLIENT_ID \
					${APP_NAME}  /bin/bash -c "tap-marketo -c $CONFIG_FILE create_keyfile --minute_offset 30; tap-marketo -c $CONFIG_FILE extract --log_only True"

mypy: build
	@echo "Running mypy for type-checking..."
	@time docker run ${APP_NAME} mypy tap_marketo --ignore-missing-imports

lint:
	@echo "Running linter..."
	@time black tap_marketo/ tests/
