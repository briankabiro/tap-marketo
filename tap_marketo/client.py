import logging
from typing import Dict, List, Optional, Sequence, Any, Iterator
from enum import Enum
from itertools import repeat

import requests


class MarketoAPIError(Exception):
    def __init__(self, response):
        self.codes = set()
        for error in response["errors"]:
            error_code = int(error["code"])
            try:
                self.codes.add(MarketoErrorCode(error_code))
            except ValueError:
                self.codes.add(error_code)

        messages = (
            error["message"] for error in response["errors"] if "message" in error
        )

        super().__init__("\n".join(messages))


class MarketoErrorCode(int, Enum):
    ACCESS_TOKEN_EXPIRED = 602
    ACCESS_TOKEN_INVALID = 601


class MarketoClient(object):
    def __init__(self, config: Dict[str, str]) -> None:
        self.endpoint: str = config["endpoint"]
        self.identity: str = config["identity"]
        self.client_id: str = config["client_id"]
        self.client_secret: str = config["client_secret"]
        self.start_time: str = config["start_time"]
        self.access_token: str = self.get_access_token()
        self.start_time_token: str = self.get_date_token()

    def renew_access_token(self, func, retries=1):
        """
        Wraps the function and ensure it will be retried if the
        access token is expired. (HTTP code 602)

        See http://developers.marketo.com/rest-api/authentication/
        """

        def decorated(*args, **kwargs):
            ret = None
            for _ in repeat(None, retries + 1):
                try:
                    return func(*args, **kwargs)
                except MarketoAPIError as err:
                    if MarketoErrorCode.ACCESS_TOKEN_EXPIRED in err.codes:
                        self.access_token = self.get_access_token()
                    elif MarketoErrorCode.ACCESS_TOKEN_INVALID in err.codes:
                        logging.critical("Access token is invalid.")
                        raise
                    else:
                        raise

        return decorated

    def chunker(self, full_list: List, chunk_size: int) -> Iterator[List]:
        """
        Generator that yields a chunk of the original list.
        """
        for i in range(0, len(full_list), chunk_size):
            yield full_list[i : i + chunk_size]

    def get_response_json(self, url: str, payload: Dict[str, Any]):
        """
        Boilerplate for GETting a request and returning the json.
        """

        auth = {"access_token": self.access_token}
        params = {**auth, **payload}
        response = requests.get(url, params=params)
        response.raise_for_status()
        return response.json()

    def check_response_success(self, response: Dict) -> Dict:
        """
        Marketo returns a 200 even if the request might have failed.
        Check the status in the object and return if truly successful.
        """

        if not response["success"]:
            logging.error(f"Request failed, errors: {response['errors']}")
            raise MarketoAPIError(response)
        else:
            return response

    def get_access_token(self) -> str:
        """
        Hit the Marketo Identity endpoint to get a valid access token.
        """

        self.access_token = ""
        logging.info("Getting access token...")
        identity_url = f"{self.identity}/oauth/token"
        payload = {
            "grant_type": "client_credentials",
            "client_id": self.client_id,
            "client_secret": self.client_secret,
        }

        return self.get_response_json(identity_url, payload)["access_token"]

    def get_date_token(self) -> str:
        """
        Get a date-based paging token from Marketo for use in other calls.
        """

        token_url = f"{self.endpoint}/v1/activities/pagingtoken.json"

        @self.renew_access_token
        def fetch_token():
            payload = {"sinceDatetime": self.start_time}
            return self.check_response_success(
                self.get_response_json(token_url, payload)
            )

        response = fetch_token()
        return response["nextPageToken"]

    def get_activities(self, activity_type_ids: List[int]) -> List[Dict]:
        """
        Get a list of activities based on a datetime nextPageToken.
        """

        logging.info("Getting activities...")
        chunk_size = 10  # This is the limit for the API
        date_token = self.start_time_token
        activities_url = f"{self.endpoint}/v1/activities.json"

        count = 0
        # GET response filtered by activity type ids
        for type_chunk in self.chunker(activity_type_ids, chunk_size):

            @self.renew_access_token
            def fetch_chunk():
                payload = {"nextPageToken": date_token, "activityTypeIds": type_chunk}
                return self.check_response_success(
                    self.get_response_json(activities_url, payload)
                )

            # Loop until there are no more results
            while True:
                response = fetch_chunk()

                if response.get("result"):
                    count += len(response["result"])
                    logging.info(f"Retrieved {count} activities total...")

                    yield response["result"]

                if response["moreResult"]:
                    date_token = response["nextPageToken"]
                else:
                    break

        return count

    def get_activity_types(self) -> List[Dict]:
        """
        Get the full list of activity types.
        """

        logging.info("Getting activity types...")
        ## TODO: Deal with the case that there are over 300 activity types
        activity_type_url = f"{self.endpoint}/v1/activities/types.json"
        payload: Dict = {}

        @self.renew_access_token
        def fetch_chunk():
            return self.check_response_success(
                self.get_response_json(activity_type_url, payload)
            )

        response = fetch_chunk()
        result = response["result"]

        logging.info(f"Retrieved {len(result)} records")
        yield result

        return len(result)

    def get_leads(self, activity_lead_ids: List[str]) -> List[Dict]:
        """
        Get lead data based on leads pulled in by the activities endpoint.
        """
        logging.info("Getting leads...")
        chunk_size = 300  # This is the limit for the API
        leads_url = f"{self.endpoint}/v1/leads.json"

        # GET response filtered by activity type ids
        count = 0
        for id_chunk in self.chunker(activity_lead_ids, chunk_size):

            @self.renew_access_token
            def fetch_chunk():
                payload = {"filterType": "id", "filterValues": id_chunk}
                return self.check_response_success(
                    self.get_response_json(leads_url, payload)
                )

            response = fetch_chunk()

            result = response["result"]
            count += len(result)

            logging.info(f"Retrieved {len(result)} leads total for...")
            yield result

        return count

    def _normalize(self, records: List[Dict]) -> List[Dict]:
        return records

    def get_all(self) -> Dict[str, int]:
        """
        Get leads, activities and activity_types.

        Yield (stream, records) for each record batch.
        :stream: Stream name
        :records: List of records

        Return {stream: count}
        :stream: Stream name
        :count: total number of records
        """
        ids = {"activity_type": [], "lead": []}
        count = {"activity": 0, "lead": 0, "activity_type": 0}
        logging.info("Starting API Calls...")

        # Retrieve all activity types
        for activity_types in self.get_activity_types():
            activity_type_ids = {record["id"] for record in activity_types}
            ids["activity_type"].extend(activity_type_ids)
            logging.info(f"Retrieved {activity_type_ids} activity_type_ids...")

            yield ("activity_types", self._normalize(activity_types))
            count["activity_type"] += len(activity_types)

        # Retrieve all related activities
        for activities in self.get_activities(ids["activity_type"]):
            activity_lead_ids = {record["leadId"] for record in activities}
            ids["lead"].extend(activity_lead_ids)
            logging.info(f"Retrieved {len(activities)} activities...")

            yield ("activities", self._normalize(activities))
            count["activity"] += len(activities)

        # Retrieve leads that are related to retrieved activities
        for leads in self.get_leads(ids["lead"]):
            logging.info(f"Retrieved {len(leads)} total leads...")

            yield ("leads", self._normalize(leads))
            count["lead"] += len(leads)

        return count
