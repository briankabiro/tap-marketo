from datetime import datetime
from os import environ as env

import pytest
from unittest import mock

from tap_marketo.client import *
from tap_marketo.marketo_utils import *


# Set up a pytest namespace so that data can be passed between tests
def pytest_namespace():
    return {}


def is_iterable(iterable):
    try:
        iter(iterable)
    except TypeError:
        return False
    return True


@pytest.fixture(scope="module")
def marketo_utils():
    return MarketoUtils(env.copy())


@pytest.fixture(scope="module")
def config(marketo_utils):
    return marketo_utils.generate_keyfile(
        minute_offset=60, output_file=None, run_time=datetime.utcnow().isoformat()
    )


class TestMarketoUtils:
    @pytest.fixture(scope="class")
    def subject(self, marketo_utils):
        return marketo_utils

    def test_create_keyfile(self, subject, config):
        assert list(config.keys()) == [
            "endpoint",
            "identity",
            "client_id",
            "client_secret",
            "start_time",
        ]

    def test_generate_start_time(self, subject):
        run_time = "2018-10-01T01:00:00.000000"
        expected_output = "2018-10-01T00:00:00Z"
        actual_output = subject.generate_start_time(offset=60, run_time=run_time)


class TestMarketoClient:
    class Context:
        def __init__(self):
            self.activity_types = []
            self.activities = []
            self.leads = []

    @pytest.fixture(scope="class")
    def ctx(self):
        return self.Context()

    @pytest.fixture(scope="class")
    def subject(self, config):
        return MarketoClient(config)

    def test_init_class(self, subject):
        assert subject.access_token is not None
        assert subject.start_time_token is not None

    def test_get_activity_types(self, subject, ctx):
        activity_types = next(subject.get_activity_types())
        head = activity_types[0]

        assert is_iterable(activity_types)
        assert type(head) == dict
        ctx.activity_types = [head["id"]]

    def test_get_activities(self, subject, ctx):
        activities = next(subject.get_activities(ctx.activity_types))
        head = activities[0]

        assert is_iterable(activities)
        assert type(head) == dict
        ctx.activities = [head["leadId"]]

    def test_get_activities_access_token_expires(self, subject, ctx):
        error = MarketoAPIError(
            {"errors": [{"code": "602", "message": "API Token is expired."}]}
        )
        results = {"result": ["a", "b", "c"]}

        with mock.patch.object(
            subject, "check_response_success", side_effect=(error, results)
        ) as check_response_success:
            activities = next(subject.get_activities(ctx.activity_types))
            assert results["result"] == activities

    def test_get_leads(self, subject, ctx):
        leads = next(subject.get_leads(ctx.activities))
        head = leads[0]

        assert is_iterable(leads)
        assert type(head) == dict
        ctx.leads = list(leads)

    def test_renew_access_token(self, subject):
        error = MarketoAPIError(
            {"errors": [{"code": "602", "message": "API Token is expired."}]}
        )
        results = {"result": ["a", "b", "c"]}

        # fmt: off
        with mock.patch.object(subject,
                               "check_response_success",
                               side_effect=(error, results)) \
                               as check_response_success, \
             mock.patch.object(subject,
                               "get_access_token",
                               return_value="DA7ABAE") \
                               as get_access_token:
        # fmt: on
            func = subject.renew_access_token(check_response_success)
            r = func()

            assert get_access_token.called
            assert subject.access_token == "DA7ABAE"
            assert r == results

        # forward other errors
        error = MarketoAPIError({
            "errors": [{
                "code": "601",
                "message": "Access token invalid."
            },{
                "code": "123",
                "message": "Another random error."
            }]
        })

        # fmt: off
        with mock.patch.object(subject,
                               "check_response_success",
                               side_effect=error) \
                               as check_response_success, \
             pytest.raises(MarketoAPIError, message="Access token invalid.\nAnother random error."):
        # fmt: on
            func = subject.renew_access_token(check_response_success)
            r = func()
